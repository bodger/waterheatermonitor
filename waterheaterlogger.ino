#include <SD.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>

#define PIN_ONE_WIRE  4
#define PIN_UPPER     3
#define PIN_LOWER     2
#define PIN_RESET     9
#define PIN_SDCS      10

#define PIN_TFT_DC    6
#define PIN_TFT_CS    5



/* Uno, Teensy SPI pins:
 *  SS   10
 *  MOSI 11
 *  MISO 12
 *  SCK  13
 */

#define DISPLAYINTERVAL   3
#define LOGINTERVAL      60

static void               error(const __FlashStringHelper * message);

static File               logfile;
static OneWire            oneWire(PIN_ONE_WIRE);
static DallasTemperature  sensors(&oneWire);
static Adafruit_ILI9341   tft = Adafruit_ILI9341(PIN_TFT_CS, PIN_TFT_DC);

static char filename[] = "LOGGER00.CSV";
static char curtimestring[16];
static char maxtimestring[16];

static const char     comma = ',';

static float          maxtemp;
static boolean        maxtemp_upper;
static boolean        maxtemp_lower;
static unsigned long  epoch;
static unsigned long  nextdisplay;
static unsigned long  nextlog;
static unsigned long  maxtime;

float
ctof(
  float c)
{
  return c * 9 / 5 + 32;
}

char
element(
  boolean state)
{
  return state ? '_' : '\xe9';
}

void
mktime(
  unsigned long days,
  char *        buf)
{
  unsigned long hours;
  unsigned long minutes;
  unsigned long seconds;

  seconds = days % 60;
  days /= 60;
  minutes = days % 60;
  days /= 60;
  hours = days % 24;
  days /= 24;

  sprintf(buf, "%dd %02d:%02d:%02d", days, hours, minutes, seconds);
}

void setup(void)
{
  byte    n;
  uint8_t val;

  Serial.begin(9600);
  Serial.println(F("Water heater monitor"));

  pinMode(PIN_UPPER, INPUT);
  pinMode(PIN_LOWER, INPUT);
  pinMode(PIN_RESET, INPUT_PULLUP);

  tft.begin();

  // read diagnostics (optional but can help debug problems)
  val = tft.readcommand8(ILI9341_RDMODE);
  Serial.print("Display Power Mode: 0x");
  Serial.println(val, HEX);
  val = tft.readcommand8(ILI9341_RDMADCTL);
  Serial.print("MADCTL Mode: 0x");
  Serial.println(val, HEX);
  val = tft.readcommand8(ILI9341_RDPIXFMT);
  Serial.print("Pixel Format: 0x");
  Serial.println(val, HEX);
  val = tft.readcommand8(ILI9341_RDIMGFMT);
  Serial.print("Image Format: 0x");
  Serial.println(val, HEX);
  val = tft.readcommand8(ILI9341_RDSELFDIAG);
  Serial.print("Self Diagnostic: 0x");
  Serial.println(val, HEX);

  tft.setRotation(1);

  tft.fillScreen(ILI9341_BLUE);
  tft.setCursor(0, 0);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(3);
  tft.println(F("water heater monitor"));

  if (!SD.begin(PIN_SDCS)) {
    error(F("SD card initialization failed"));
  }

  Serial.println(F("SD card initialized"));

  for (n = 0; n < 100; ++n) {
    Serial.print(F("trying file "));
    Serial.println(n);
    filename[6] = n / 10 + '0';
    filename[7] = n % 10 + '0';

    if (!SD.exists(filename)) {
      Serial.print(F("creating "));
      Serial.println(filename);
      logfile = SD.open(filename, FILE_WRITE);
      Serial.println(F("opened file"));
      tft.println(filename);
      break;
    }
  }

  Serial.println(F("exited log loop"));

  if (!logfile) {
    error(F("couldn't create logfile"));
  }

  Serial.print(F("logging to "));
  Serial.println(filename);

  sensors.begin();
  epoch = millis() / 1000;

  mktime(0, maxtimestring);

  logfile.println(F("seconds,upper,lower,temp"));

  Serial.println(maxtimestring);
  Serial.println("initialized");
}

void loop(void)
{
  float         curtemp;
  boolean       upper;
  boolean       lower;
  unsigned long curtime;
  unsigned long timestamp;

  curtime = millis() / 1000;

  if (!digitalRead(PIN_RESET)) {
    maxtemp       = 0;
    maxtemp_upper = 0;
    maxtemp_lower = 0;
    nextdisplay   = 0;
    nextlog       = 0;
    epoch         = curtime;
    maxtime       = curtime;
  }

  if (curtime > nextdisplay) {
    sensors.requestTemperatures();
    curtemp = sensors.getTempCByIndex(0);
    upper   = digitalRead(PIN_UPPER);
    lower   = digitalRead(PIN_LOWER);

    timestamp = curtime - epoch;

    if (curtemp > maxtemp) {
      maxtemp       = curtemp;
      maxtemp_upper = upper;
      maxtemp_lower = lower;
      maxtime       = curtime;

      mktime(maxtime - epoch, maxtimestring);
    }

    mktime(timestamp, curtimestring);

    Serial.print(timestamp);
    Serial.print(comma);
    Serial.print(curtimestring);
    Serial.print(' ');
    Serial.print(upper);
    Serial.print(comma);
    Serial.print(lower);
    Serial.print(comma);
    Serial.println(curtemp);

    tft.fillScreen(ILI9341_BLACK);
    tft.setCursor(0, 0);
    tft.setTextColor(ILI9341_GREEN);
    tft.setTextSize(3);
    tft.println(curtimestring);
    tft.print("cur ");
    tft.print(ctof(curtemp));
    tft.print("\xf7 F ");
    tft.print(element(upper));
    tft.println(element(lower));

    tft.print("max ");
    tft.print(ctof(maxtemp));
    tft.print("\xf7 F ");
    tft.print(element(maxtemp_upper));
    tft.println(element(maxtemp_lower));
    tft.println(maxtimestring);

    if (curtime > nextlog) {
      logfile.print(curtime - epoch);
      logfile.print(comma);
      logfile.print(upper);
      logfile.print(comma);
      logfile.print(lower);
      logfile.print(comma);
      logfile.println(curtemp);
      logfile.flush();

      nextlog = curtime + LOGINTERVAL;
    }

    nextdisplay = curtime + DISPLAYINTERVAL;
  }
}

static void
error(
  const __FlashStringHelper *  message)
{
  Serial.println(message);

  tft.fillScreen(ILI9341_RED);
  tft.setCursor(0, 0);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(1);
  tft.println(message);
 
  for (;;) {
  }
}
