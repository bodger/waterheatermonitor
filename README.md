# waterheatermonitor

I'd had an issue with my water heater's overtemperature cutout operating, but I didn't know which part was failing.  The water heater has two thermostats and two heating elements, and a failure of any of them could cause this (the heating elements can develop an internal short to ground that causes them to continue to draw current and heat water even if the thermostat is off).

Accordingly, I set up an Arduino with a temperature sensor and two voltage monitors connected to the heating elements.  While I realize that a short in an element could cause false readings, I figured this would be more or less constant, but if I saw cycling, it was likely the thermostat.

I connected an Arduino to the temperature sensor and a shield containing an LCD display and SD card socket so I could display the current and max temperature and the state of the thermostats, as well as log the status over time.

It did work, I determined that the lower thermostat would sometimes stick on, and then the water would heat up until the overtemperature safety tripped.  I replaced the lower thermostat and it works fine again now.

